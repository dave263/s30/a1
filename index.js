const express = require('express');

const mongoose = require('mongoose');

const dotenv = require('dotenv');
dotenv.config();

const app = express();

const PORT = 3001;


app.use(express.json())
app.use(express.urlencoded({extended:true}))

mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

const db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log('Connected to Database'))


const userSchema = new mongoose.Schema(
    {
        userName: {
            type: String,
            required: [true, `Username is required`]
        },
        password: {
            type: String,
            default: [true, "Password is required"]
        }
    }
)

const User = mongoose.model('User', userSchema)

app.post('/signup', (req, res) => {
    console.log(req.body)
    const {username, password} = req.body
    res.send(`User ${username} has been registered`)
        }
)
app.listen(PORT, () => console.log(`Server connected at port ${PORT}`))